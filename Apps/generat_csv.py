#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved
import os
import sys

# Require when you haven't GLXRadio as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

import GLXRadio

my_csv_info = list()
#########################################
# PMR446                                #
#########################################

generator_pmr446 = GLXRadio.PMR446()
generator_pmr446.suffix_name = 'PMR'

# Phonie / Survivaliste / Preppers
for info_channel in generator_pmr446.get_info_channels(
        channels=[1, 2, 3, 4],
        tones=None
):
    my_csv_info.append(info_channel)

# Phonie - Canal Scout - Randonnée / Radioscoutisme
for info_channel in generator_pmr446.get_info_channels(
        channels=[5],
        tones=[1, 2, 3, 4, 5, 6, 7, 8, 42]
):
    my_csv_info.append(info_channel)

# Répéteurs / CTCSS Recommandé
for info_channel in generator_pmr446.get_info_channels(
        tone='CTCSS',
        channels=[6],
        tones=None
):
    my_csv_info.append(info_channel)

# Balises/Modes Digitaux/URGENCE MONTAGNE / URG. MONTAGNE = Ch 7 tone 7
for info_channel in generator_pmr446.get_info_channels(
        channels=[7],
        tones=[7]
):
    my_csv_info.append(info_channel)

# CANAL D'APPEL (Local et DX) / Aucun CTCSS ou 8
for info_channel in generator_pmr446.get_info_channels(
        channels=[8],
        tones=[8]
):
    my_csv_info.append(info_channel)

csv_generator = GLXRadio.CSV()

# With location
info_with_location = csv_generator.add_location(my_csv_info)

# With Frist line
info_with_first_line = csv_generator.add_first_line(info_with_location)

# Print
csv_generator.flush(info_with_first_line)

sys.exit(0)
