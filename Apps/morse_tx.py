#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import sys
import os
import tempfile

# Require when you haven't GLXRadio as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

from glxradio.morse.encoder import MorseEncoder
from glxaudio.AudioPlayer import AudioPLayer
from glxviewer import viewer
# THE APP
debug = False
debug_level = 0

temporary_file = tempfile.NamedTemporaryFile()
temporary_filename = temporary_file.name

try:

    while True:
        # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
        # Yes look strange but permit to close the temp file after a KeyboardInterrupt ;)
        temporary_file.close()
        temporary_file = tempfile.NamedTemporaryFile()
        temporary_filename = temporary_file.name

        viewer.write(
            status_text='WAIT',
            status_text_color='WHITE',
            status_symbol='<',
            prompt=-1,
            column_1=">"
        )

        message = input().strip()
        if not message:
            continue

        with MorseEncoder() as morse_encoder:
            # Just for the demo
            morse_encoder.set_debug(debug)
            morse_encoder.set_debug_level(debug_level)
            morse_encoder.set_verbose(False)

            message_encode = morse_encoder.encode(message)

            # viewer.flush_infos(
            #     status_text='MORSE',
            #     status_text_color='WHITE',
            #     column_1='Code: ' + str(message_encode)
            # )

            # Display Morse code with fun
            # morse_encoder.set_dit_sign('▄')
            # morse_encoder.set_dah_sign('▄▄▄')
            # message_encode_exact = morse_encoder.encode_exact(message)
            # viewer.flush_infos(
            #     status_text='MORSE',
            #     status_text_color='WHITE',
            #     column_1='Code: ' + str(message_encode_exact)
            # )

            # Save the wave file
            morse_encoder.text_to_morse_wave(message, temporary_filename)

        # Play the sound for transmission (Not really a MorseEncoder transmission,
        # just play a valid MorseEncoder code into Wave file)
        with AudioPLayer() as player:
            # Just for the demo
            player.set_debug(debug)
            player.set_debug_level(debug_level)
            player.set_is_detached(False)

            duration = player.play(
                filename=temporary_file.name
            )

            element = morse_encoder.get_message_length(message)
            element = int(round(element))

            line_info = 'Send '
            line_info += str(int(element))
            line_info += ' element(s) in '
            line_info += str(round(duration, ndigits=3)) + 's'
            line_info += ', '
            if duration == 0:
                line_info += '0'
            else:
                line_info += str(round(5 * 0.240 / duration * element, ndigits=3))
            line_info += 'WPM'
            line_info += ', '
            if duration == 0:
                line_info += '0'
            else:
                line_info += str(int(round(duration / element, ndigits=3) * 1000))
            line_info += 'ms/element'

            viewer.write(
                status_text='MORSE',
                status_text_color='WHITE',
                column_1=line_info,
            )

        # with MorseDecoder() as morse_decoder:
        #     # Just for the demo
        #     morse_decoder.set_debug(debug)
        #     morse_decoder.set_debug_level(debug_level)
        #
        #     # Analyse the wav file
        #     decoded_message = morse_decoder.process(temporary_filename)
        #
        #     # viewer.write(
        #     #     status_text='MORSE',
        #     #     status_text_color='WHITE',
        #     #     column_1='Decoded message: ',
        #     #     column_2=str(decoded_message),
        #     #     prompt=-1
        #     # )

except KeyboardInterrupt:
    # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
    viewer.write(
        status_text='APP',
        status_text_color='WHITE',
        column_1='Delete temporary file: ',
        column_2=temporary_file.name
    )
    temporary_file.close()
    sys.exit(0)
except EOFError:
    # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
    viewer.write(
        status_text='APP',
        status_text_color='WHITE',
        column_1='Delete temporary file: ',
        column_2=temporary_file.name
    )
    temporary_file.close()
    sys.exit(0)
