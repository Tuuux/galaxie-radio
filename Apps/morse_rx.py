#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import sys
import os
import tempfile

# Require when you haven't GLXRadio as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

from glxradio.morse.decoder import MorseDecoder
from glxaudio.AudioRecorder import AudioRecorder
from glxaudio.AudioConstants import GLXAUDIO
from glxviewer import viewer
# THE APP
debug = False
debug_level = 0

temporary_file = tempfile.NamedTemporaryFile()
temporary_filename = temporary_file.name

try:

    while True:
        # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
        # Yes look strange but permit to close the temp file after a KeyboardInterrupt ;)
        temporary_file.close()
        temporary_file = tempfile.NamedTemporaryFile()
        temporary_filename = temporary_file.name

        with AudioRecorder() as recorder:
            recorder.set_debug(debug)
            recorder.set_debug_level(debug_level)
            recorder.set_format(GLXAUDIO.FORMAT_INT16)
            recorder.set_threshold(5)  # in percent
            recorder.set_channels(1)
            recorder.set_rate(8000)
            recorder.set_chunk_size(2048)
            recorder.record_to_file(filename=temporary_file.name)
                
        with MorseDecoder() as morse_decoder:
            # Just for the demo
            morse_decoder.set_debug(True)
            morse_decoder.set_channels(1)
            morse_decoder.set_rate(8000)
            morse_decoder.set_format(GLXAUDIO.FORMAT_INT16)
            morse_decoder.set_debug_level(debug_level)
            morse_decoder.set_chunk_size(2048)

            # Analyse the wav file
            # decoded_message = morse_decoder.process(temporary_filename)

            viewer.write(
                status_text='RX',
                status_text_color='RED',
                column_1=str(morse_decoder.process(temporary_filename)),
                prompt=-1,
            )

except KeyboardInterrupt:
    # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
    viewer.write(
        status_text='APP',
        status_text_color='WHITE',
        column_1='Delete temporary file: ',
        column_2=temporary_file.name
    )
    temporary_file.close()
    sys.exit(0)
except EOFError:
    # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
    viewer.write(
        status_text='APP',
        status_text_color='WHITE',
        column_1='Delete temporary file: ',
        column_2=temporary_file.name
    )
    temporary_file.close()
    sys.exit(0)
