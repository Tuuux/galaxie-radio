#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import sys
import os
import tempfile

# Require when you haven't GLXRadio as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

from glxaudio.AudioRecorder import AudioRecorder
from glxaudio.AudioPlayer import AudioPLayer
from glxaudio.AudioInterfaces import AudioInterfaces
from glxaudio.AudioConstants import GLXAUDIO
from glxradio.libs.sleep import Sleep
from glxviewer import viewer

# THE APP
try:
    verbose = True
    debug = True
    debug_level = 2

    time_to_sleep = 0.42

    if verbose:
        viewer.flush_infos(
            status_text='INIT',
            status_text_color='WHITE',
            column_1='Simplex Repeater',
            column_2=' - Version 0.5'
        )

        viewer.flush_infos(
            status_text='INIT',
            status_text_color='WHITE',
            column_1=AudioInterfaces.__name__ + ':',
            column_2='list interfaces'
        )
        AudioInterfaces.print_interfaces(only_default=True)

    while True:
        #  Create a new temporary file each time, that because communication's should be anonyme
        temporary_file = tempfile.NamedTemporaryFile()
        try:
            # Start a recording
            with AudioRecorder() as recorder:
                recorder.set_debug(debug)
                recorder.set_debug_level(debug_level)
                recorder.set_verbose(verbose)
                recorder.set_format(GLXAUDIO.FORMAT_INT16)
                recorder.set_threshold(5)  # in percent
                recorder.set_channels(1)
                recorder.set_rate(22050)
                recorder.set_chunk_size(1024)
                recorder.record_to_file(filename=temporary_file.name)

            # Wait , because that is how work a repeater
            with Sleep() as sleeper:
                sleeper.set_debug(debug)
                sleeper.set_debug_level(debug_level)
                sleeper.set_verbose(verbose)
                sleeper.sleep(time_to_sleep)

            # Play what is inside our temporary file
            with AudioPLayer() as player:
                player.set_debug(debug)
                player.set_debug_level(debug_level)
                player.set_verbose(verbose)
                try:
                    player.play(filename=temporary_file.name)
                except Exception as error:
                    viewer.flush_infos(
                        status_text='ERROR',
                        status_text_color='RED',
                        column_1="{0}".format(error),
                    )


        except EOFError:
            pass

        # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
        temporary_file.close()

except KeyboardInterrupt:
    sys.exit(0)
