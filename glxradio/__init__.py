#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

APPLICATION_AUTHORS = ["Tuuuux"]
APPLICATION_VERSION = '0.2'


from glxradio.morse.code import MorseCode
from glxradio.morse.encoder import MorseEncoder
from glxradio.morse.decoder import MorseDecoder

# Inspirate by: https://www.pervisell.com/ham/hc1.htm

