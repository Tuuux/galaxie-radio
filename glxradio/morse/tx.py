import sys
import tempfile
from cmd import Cmd
import term
from glxaudio.AudioPlayer import AudioPLayer
from glxviewer import viewer
from glxradio.morse.encoder import MorseEncoder

#  from glxradio.libs.parser import parser_frequency


class MorseTX(Cmd):
    prompt = "> "
    intro = "*************************** GALAXIE RADIO - MORSE TX ***************************\n"

    def __init__(self):
        super().__init__()
        self.morse_encoder = MorseEncoder()

    def do_EOF(self, line):
        term.writeLine()
        return True

    def emptyline(self):
        pass

    def parseline(self, line):
        """Parse the line into a command name and a string containing
        the arguments.  Returns a tuple containing (command, args, line).
        'command' and 'args' may be None if the line couldn't be parsed.
        """
        line = line.strip()
        if not line:
            return None, None, line
        elif line[0] == "/":
            line = line[1:]
            i, n = 0, len(line)
            while i < n and line[i] in self.identchars:
                i = i + 1
            cmd, arg = line[:i], line[i:].strip()
            return cmd, arg, line
        else:
            with tempfile.NamedTemporaryFile() as temporary_file:
                # temporary_filename = temporary_file.name
                # message_encode = self.morse_encoder.encode(line)
                # Save the wave file
                self.morse_encoder.text_to_morse_wave(line, temporary_file.name)
                with AudioPLayer() as player:
                    player.set_rate(8000)
                    # Just for the demo
                    duration = player.play(filename=temporary_file.name)
                    element = int(round(self.morse_encoder.get_message_length(line)))
                    line_info = "Send "
                    line_info += str(element)
                    line_info += " element(s) in "
                    line_info += str(round(duration, ndigits=3)) + "s"
                    line_info += ", "
                    if duration == 0:
                        line_info += "0"
                    else:
                        line_info += str(round(1.20 / duration * element, ndigits=3))
                    line_info += "WPM"
                    line_info += ", "
                    if duration == 0:
                        line_info += "0"
                    else:
                        line_info += str(int(round(duration / element, ndigits=3) * 1000))
                    line_info += "ms/element"

                    viewer.write(
                        status_text="MORSE",
                        status_text_color="WHITE",
                        column_1=line_info,
                    )
            return None, None, line

    def default(self, line: str) -> bool:
        pass

    def do_exit(self, *args):
        return True

    def do_set(self, line):
        args = line.split()
        if args:
            if args[0] == "frequency":
                try:
                    self.morse_encoder.frequency = float(args[1])
                    viewer.write(
                        status_text="SET",
                        status_text_color="YELLOW",
                        status_symbol=">",
                        column_1="{0}".format(line),
                    )
                except Exception:
                    viewer.write(
                        status_text="SET",
                        status_text_color="RED",
                        column_1="Unknow frquency parameter",
                    )
            elif args[0] == "wpm":
                try:
                    self.morse_encoder.dot_dur = 1.20 / float(args[1])
                    viewer.write(
                        status_text="SET",
                        status_text_color="YELLOW",
                        status_symbol=">",
                        column_1="{0}".format(line),
                    )
                except Exception:
                    viewer.write(
                        status_text="SET",
                        status_text_color="RED",
                        column_1="Unknow duration parameter",
                    )


def main():
    if len(sys.argv) > 1:
        sys.exit(MorseTX().onecmd(" ".join(sys.argv[1:])))
    else:
        sys.exit(MorseTX().cmdloop())


if __name__ == "__main__":
    main()
