#!/usr/bin/env python3
# morse_encoder.py--  morse_encoder decoder
#
# Copyright (C) 2014   Mauri Niininen, AG1LE
# Copyright (C) 2019   Galaxie Radio Team, TUXA
#
#
# bmorse.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# morse_encoder.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with bmorse.py.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from glxaudio.AudioConstants import (
    GLXAUDIO,
)
from glxradio.libs.frequency import (
    Frequency,
)
from glxradio.morse.code import (
    MorseCode,
)
from glxaudio.Audio import (
    Audio,
)
from glxviewer import (
    Viewer,
)
import numpy as np
import pyaudio
import math
from scipy import (
    signal,
)
import wave

# from optparse import OptionParser
from collections import (
    deque,
)
import warnings

warnings.filterwarnings(
    "ignore",
    category=FutureWarning,
)


class MorseDecoder(
    MorseCode,
    Frequency,
    Audio,
):

    # initialize MorseEncoder object
    def __init__(
        self,
    ):
        MorseCode.__init__(self)
        Frequency.__init__(self)
        Audio.__init__(self)
        # Debug
        self.verbose = False
        self.debug_level = 0
        # self.wav_file = None
        self.wpm_default = None
        self.wpm_upper = None
        self.wpm_lower = None
        self.threshold_upper = None
        self.threshold_lower = None
        self.auto_gain_control = None
        self.auto_gain_control_queue = None
        self.auto_gain_control_queue_size = None
        self.dit_size = None
        self.two_dits_size = None
        self.sigma = None
        self.cws = None
        self.what_i_display = None
        #############################################
        # Set Audio information's for the first time
        #############################################
        # GLXAUdio settings
        self.format = GLXAUDIO.FORMAT_INT16
        self.channels = 1
        self.rate = 8000
        # Internal settings
        self.set_sigma(0.4)  # used in PNN: 0.3 .. 0.4  produces good results
        self.set_wpm_default(20)
        self.set_wpm_upper(60)
        self.set_wpm_lower(5)
        self.set_threshold_upper(50)
        self.set_threshold_lower(50)
        # Dit and size thing
        # self.set_dit_size((0.0*25)*1000)
        # https://www.qsl.net/yo3fca/hstc.htm
        self.set_dit_size(1200)
        # assume 8 KHz sample rate
        self.set_two_dits_size(2 * self.get_dit_size() * (self.get_rate() / 1000) / self.get_wpm_default())
        # cw string to collect . and - based on symbols received
        self.set_cws()
        self.set_what_i_display()
        #
        # International support
        #
        self.use_international_code = True
        # Internal
        self.last = 0
        self.lastmark = 0
        self.mark = 0
        self.space = 0
        self.ticks = 0
        #
        # Auto Gain Control
        #
        self.set_auto_gain_control(False)
        self.set_auto_gain_control_queue(deque(""))
        self.set_auto_gain_control_queue_size(10)
        self.get_auto_gain_control_rolling(self.get_two_dits_size())
        #
        # 40 msec in # of samples
        self.dit_low_limit = 2 * self.get_dit_size() / self.get_wpm_upper()
        # 240 msec in # of samples
        self.dit_high_limit = 2 * self.get_dit_size() / self.get_wpm_lower()

        self.viewer = Viewer()

    def __enter__(
        self,
    ):
        return self

    def __exit__(
        self,
        exc_type,
        exc_val,
        exc_tb,
    ):
        pass
        # self.close()

    # Low Level
    def set_what_i_display(
        self,
        text="",
    ):
        """
        Internal function for store the text actually received.

        Note it can be request during a process by self.get_what_i_display()

        :param text: the text actually received
        :type text: str
        :raise TypeError: when "text" argument is not a :py:data:`string`
        """
        # Exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' text be a str")

        # make the job in case
        if self.get_what_i_display() != str(text):
            self.what_i_display = str(text)

    def get_what_i_display(
        self,
    ):
        """
        Used for get text based on symbols received, can be request during a
        process

        :return: the text actually received
        :rtype: str
        """
        # make the job in case
        return self.what_i_display

    def set_cws(
        self,
        text="",
    ):
        """
        cw string to collect . and - based on symbols received

        Used for add and remove detected char or word string


        :param text: the text to set
        :type text: str
        :raise TypeError: when "text" argument is not a :py:data:`string`
        """
        # Exit as soon of possible
        if type(text) != str:
            raise TypeError("'char' text be a str")

        # make the job in case
        if self.get_cws() != str(text):
            self.cws = str(text)

    def get_cws(
        self,
    ):
        """
        Used for get text based on symbols received

        :return: the text , as set by set_cws()
        :rtype: str
        """
        # make the job in case
        return self.cws

    def set_two_dits_size(
        self,
        twodits=None,
    ):
        """
        used

        Default: 2 * self.get_dit_size() * (self.get_rate() / 1000) /
        self.get_wpm_default()


        :param twodits: two_dits_size in ms
        :type twodits: int or float
        :raise TypeError: when "value" argument is not `int` or `float` type
        """
        # Exit as soon of possible
        if type(twodits) != int and type(twodits) != float:
            raise TypeError("'two_dits_size' must be a int or float type")

        # make the job in case
        if self.get_two_dits_size() != float(twodits):
            self.two_dits_size = float(twodits)

    def get_two_dits_size(
        self,
    ):
        """

        Used somewhere

        :return: of two_dits_size , as set by set_twodits
        :rtype: float
        """
        # make the job in case
        return self.two_dits_size

    def set_sigma(
        self,
        value=0.35,
    ):
        """
        used in PNN: 0.3 .. 0.4  produces good results

        Default: 0.35


        :param value: Dit length in WPM ms
        :type value: int or float
        :raise TypeError: when "value" argument is not `int` or `float` type
        """
        # Exit as soon of possible
        if type(value) != int and type(value) != float:
            raise TypeError("'value' must be a int or float type")

        # make the job in case
        if self.get_sigma() != float(value):
            self.sigma = float(value)

    def get_sigma(
        self,
    ):
        """
        Return the value of sigma , as set by set_sigma

        used in PNN: 0.3 .. 0.4  produces good results

        Default: 0.35

        :return: value of sigma , as set by set_sigma
        :rtype: float
        """
        # make the job in case
        return self.sigma

    def set_dit_size(
        self,
        dit_size=1200,
    ):
        """
        Set Dit length in WPM msec.

        Default: 1200

        DIT_MAGIC = 1200  # Dit length is 1200/WPM msec

        :param dit_size: Dit length in WPM ms
        :type dit_size: int or float
        :raise TypeError: when "dit_size" argument is not `int` or `float` type
        """
        # Exit as soon of possible
        if type(dit_size) != int and type(dit_size) != float:
            raise TypeError("'dit_size' must be a int or float type")

        # make the job in case
        if self.get_dit_size() != float(dit_size):
            self.dit_size = float(dit_size)

    def get_dit_size(
        self,
    ):
        """
        DIT_MAGIC = 1200  # Dit length is 1200/WPM msec

        :return: Dit length in WPM ms
        :rtype: int or float
        """
        return self.dit_size

    def set_wpm_default(self, wpm=35):
        """
        Set the wpm_default. It is use by the init()

        Default: 35

        DEFAULT_WPM = 35  # WPM = 1.2*samplerate / (two_dits_size/2)

        :param wpm: PARIS Word peer minutes
        :type wpm: int or float
        :raise TypeError: when ``wpm`` argument is not ``int`` or ``float`` \
        type
        """
        # Exit as soon of possible
        if type(wpm) != int and type(wpm) != float:
            raise TypeError("'wpm_default' must be a int or float type")

        # make the job in case
        if self.get_wpm_default() != float(wpm):
            self.wpm_default = float(wpm)

    def get_wpm_default(
        self,
    ):
        """
        Return the default WPM.

        See: MorseDecoder.set_wpm_default()

        :return: the PARIS Word peer minutes
        :rtype: float
        """
        return self.wpm_default

    def set_wpm_upper(self, wpm=60):
        """
        Set the wpm_default. It is use by the init()

        Default: 60

        UPPER_WPM = 60  # maximum speed

        :param wpm: PARIS Word peer minutes
        :type wpm: int or float
        :raise TypeError: when ``wpm`` argument is not ``int`` or ``float`` \
        type
        """
        # Exit as soon of possible
        if type(wpm) != int and type(wpm) != float:
            raise TypeError("'wpm' must be a int or float type")

        # make the job in case
        if self.get_wpm_upper() != float(wpm):
            self.wpm_upper = float(wpm)

    def get_wpm_upper(
        self,
    ):
        """
        Return the Upper WPM us by the init()

        See: MorseDecoder.set_wpm_upper()

        :return: the PARIS Word peer minutes
        :rtype: float
        """
        return self.wpm_upper

    def set_wpm_lower(self, wpm=5):
        """
        Set the Lower WPM Limit. It is use by the init()

        Default: 5

        LOWER_WPM = 5  # minimum speed

        :param wpm: PARIS Word peer minutes
        :type wpm: int or float
        :raise TypeError: when ``wpm`` argument is not a ``int`` or ``float`` \
        type
        """
        if type(wpm) != int and type(wpm) != float:
            raise TypeError("'wpm' must be a int or float type")

        # make the job in case
        if self.get_wpm_lower() != float(wpm):
            self.wpm_lower = float(wpm)

    def get_wpm_lower(
        self,
    ):
        """
        Return the Lower WPM us by the init()

        See: MorseDecoder.set_wpm_lower()

        :return: the PARIS Word peer minutes
        :rtype: float
        """
        return self.wpm_lower

    def set_threshold_upper(
        self,
        value=50,
    ):
        """
        Set the Upper threshold. It is use by the init()

        Default: 50

        Originally UPPER_THRESHOLD = 0.5 but have been adapt for be percent
         value
        Now UPPER_THRESHOLD = 0.5/100

        :param value: Threshold in percent
        :type value: int or float
        :raise TypeError: when ``value`` is not a ``int`` or ``float`` type
        """
        # Exit as soon of possible
        if type(value) != int and type(value) != float:
            raise TypeError("'wpm' must be a int or float type")

        # Value is clamp in range 0..100
        value = self._clamp(
            value,
            0,
            100,
        )

        # make the job in case
        if self.get_threshold_upper() is None:
            self.threshold_upper = float(value / 100)
            return
        elif self.get_threshold_upper() != float(value / 100):
            self.threshold_upper = float(value / 100)

    def get_threshold_upper(
        self,
    ):
        """
        Return the Upper threshold

        Originally UPPER_THRESHOLD = 0.5 but have been adapt for be percent \
        value

        Now UPPER_THRESHOLD = 0.5*100

        :return: Upper threshold in percentage
        :rtype: float
        """
        return self.threshold_upper

    def set_threshold_lower(
        self,
        value=50,
    ):
        """
        Set the Lower threshold. It is use by the init()

        Default: 50

        Originally LOWER_THRESHOLD = 0.5 but have been adapt for be percent value
        Now LOWER_THRESHOLD = 0.5/100

        :param value: Threshold in percent
        :type value: int or float
        :raise TypeError: when ``value`` is not a ``int`` or ``float`` type
        """
        # Exit as soon of possible
        if type(value) != int and type(value) != float:
            raise TypeError("'wpm' must be a int or float type")

        # Value is clamp in range 0..100
        value = self._clamp(
            value,
            0,
            100,
        )

        # make the job in case
        if self.get_threshold_lower() is None:
            self.threshold_lower = float(value / 100)
            return
        elif self.get_threshold_lower() != float(value / 100):
            self.threshold_lower = float(value / 100)

    def get_threshold_lower(
        self,
    ):
        """
        Return the Lower threshold

        Originally LOWER_THRESHOLD = 0.5 but have been adapt for be percent value

        Now LOWER_THRESHOLD = 0.5*100

        :return: Lower threshold in percentage
        :rtype: float
        """
        return self.threshold_lower

    def add_char(
        self,
        character,
    ):
        """
        Append a character to self.cws

        :param character: A character
        :type character: str
        """
        self.set_cws(str(self.get_cws() + character))
        # sys.stdout.write(ch)
        # sys.stdout.flush()

    def print_char(
        self,
        character,
    ):
        """
        Print a character space detected

        :param character: A character
        :type character: str
        """
        self.add_char(character)

        # try to find sequence from morse_code_table_reversed
        try:
            val = self.table_reversed[self.get_cws()]
        except KeyError:
            # output '*' when cannot find sequence
            # from morse_code_table_reversed
            val = "*"

        self.set_what_i_display(str(self.get_what_i_display() + str(val)))

        # Reset cws
        self.set_cws()

    def print_word(
        self,
        character,
    ):
        """
        Print last character in word

        :param character: A character
        :type character: str
        """
        # word space detected
        # print last character in word
        self.print_char(character)
        self.set_what_i_display(str(self.get_what_i_display() + " "))

        # print word space
        # if self.get_verbose():
        #     sys.stdout.write(' ')
        #     sys.stdout.flush()

    def set_auto_gain_control(
        self,
        value=False,
    ):
        """
        Set Auto Gain Control

        Default: False

        ``True`` for enable the Auto Gain Control, ``False`` for disable it.

        See: MorseDecoder.get_auto_gain_control() for get that value

        :param value: True for enable, False for disable
        :type value: bool
        :raise TypeError: when ``value`` argument is not a :py:data:`bool`
        """
        # Exit as soon of possible
        if type(value) != bool:
            raise TypeError("'bool' must be a bool type")

        # make the job in case
        if self.get_auto_gain_control() != value:
            self.auto_gain_control = value

    def get_auto_gain_control(
        self,
    ):
        """
        Return the auto Gain Control Queue size.

        That correspond to the number of element from where get the average.

        See: MorseDecoder.get_auto_gain_control_rolling() for get the average

        :return: Queue Size
        :rtype: int
        """
        return self.auto_gain_control

    def set_auto_gain_control_queue(
        self,
        queue=None,
    ):
        """
        Set the Auto Gain Control queue.

        Default: None

        That is for store a deque Queue , where ``queue`` is set to None , a
        fresh Queue replace the actual one.

        That Queue is use by MorseDecoder.get_auto_gain_control_queue() for
        store value and can return a average value.

        See: MorseDecoder.get_auto_gain_control_rolling() for get the
        average value.

        :param queue: The Queue
        :type queue: deque
        :raise TypeError: when ``queue`` argument is not a :py:data:`deque`
        """
        if queue is None:
            self.auto_gain_control_queue = deque()
            return

        if type(queue) != deque:
            raise TypeError("'queue' must be a deque type")

        if self.get_auto_gain_control_queue != queue:
            self.auto_gain_control_queue = queue

    def get_auto_gain_control_queue(
        self,
    ):
        """
        Return the Auto gain control Queue object.

        :return: A queue use by Auto gain control
        :rtype: deque
        """
        return self.auto_gain_control_queue

    def set_auto_gain_control_queue_size(
        self,
        queue_size=10,
    ):
        """
        Set the Auto Gain Control queue size.

        Default: 10

        That correspond to the number of element from where get the average.

        See: MorseDecoder.get_auto_gain_control_rolling() for get the average

        :param queue_size: The Queue Size
        :type queue_size: int
        :raise TypeError: when ``queue_size`` argument is not a :py:data:`int`
        """
        # Exit as soon of possible
        if type(queue_size) != int:
            raise TypeError("'queue_size' must be a int type")

        # make the job in case
        if self.get_auto_gain_control_queue_size() != queue_size:
            self.auto_gain_control_queue_size = queue_size

    def get_auto_gain_control_queue_size(
        self,
    ):
        """
        Return the auto Gain Control Queue size.

        That correspond to the number of element from where get the average.

        See: MorseDecoder.get_auto_gain_control_rolling() for get the average

        :return: Queue Size
        :rtype: int
        """
        return self.auto_gain_control_queue_size

    def get_auto_gain_control_rolling(self, value):
        """
        Get the Average value of the Auto Gain Control

        Adapted from
          http://www.raspberrypi.org/forums/viewtopic.php?f=32&t=69797

        :param value: A fresh value
        :type value: int or float
        :return: Returns a simple rolling average of \
        auto_gain_control_queue_size most recent values
        :rtype: float
        """
        # if the auto_gain_control_queue is empty
        # then fill it with values of value
        if self.get_auto_gain_control_queue() == deque([]):
            for i in range(self.get_auto_gain_control_queue_size()):
                self.get_auto_gain_control_queue().append(value)
        self.get_auto_gain_control_queue().append(value)
        self.get_auto_gain_control_queue().popleft()

        # Check for average value
        average = 0
        for i in self.get_auto_gain_control_queue():
            average += i
        average = average / float(self.get_auto_gain_control_queue_size())

        # Debug section
        if self.get_debug_level() > 0:
            count = 0
            for i in self.get_auto_gain_control_queue():
                long_string = ""
                long_string += "Rolling Auto Gain Control: "
                long_string += "Num:" + str(count)
                long_string += " "
                long_string += "Value:" + str(i)
                self.viewer.write(column_2=long_string)
                count += 1

            self.viewer.write(column_2="Rolling Auto Gain Control: Average: %f" % average)

        # We return a thing that because it's cool to return something
        return float(average)

    @staticmethod
    def get_auto_gain_control_decay(
        average,
        value,
        weight,
    ):
        """
        Returns Auto Gain Control decay values

        :param average: Average value
        :type average: int or float
        :param value: Value to estimate
        :type value: int or float
        :param weight: Weight
        :type weight: int or float
        :return: Auto Gain Control decay
        :rtype: float
        """
        if weight <= 1.0:
            return float(value)
        else:
            return float(value * (1.0 / weight) + average * (1.0 - (1.0 / weight)))

    def get_probabilistic_neural_network_value(self, m, s):
        """
        Probabilistic Neural Network - find best matching symbol from mark,space duration pair

        :param m:
        :param s:
        :return:
        """

        # Symbols are defined by [mark, space] duration examples
        # Classes are normalized: dit = 0.1  dah = 0.3 char space =0.3 wordspace = 0.7
        # Adding more timing examples may help in accuracy
        # Class S0        S1         S2        S3        S4       S5          S6 noise    S7 noise S8 noise
        w = [
            [
                0.1,
                0.1,
            ],
            [
                0.1,
                0.3,
            ],
            [
                0.1,
                0.7,
            ],
            [
                0.3,
                0.1,
            ],
            [
                0.3,
                0.3,
            ],
            [
                0.3,
                0.7,
            ],
            [
                0.00,
                0.05,
            ],
            [
                0.000,
                0.5,
            ],
            [
                0.0,
                0.8,
            ],
        ]

        resval = np.linspace(
            0,
            1,
            num=9,
        )
        for i in range(0, 9):  # go through examples for each class in w[]
            v = 0.0
            # PATTERN layer - calculates PDF function for each class
            v = (
                v
                + pow(
                    m - w[i][0],
                    2,
                )
                + pow(
                    s - w[i][1],
                    2,
                )
            )
            v = math.exp(
                -v
                / (
                    2
                    * pow(
                        self.get_sigma(),
                        2,
                    )
                )
            )
            resval.flat[i] = v

            if self.get_debug() and self.get_debug_level() > 2:
                self.viewer.write(
                    column_1="Probabilistic Neural Network: m%f s%f pnn[%d] %f"
                    % (
                        m,
                        s,
                        i,
                        v,
                    )
                )

        # OUTPUT layer - select best match
        val = np.nanargmax(resval)

        if self.debug and self.debug_level > 2:
            self.viewer.write(column_1="Probabilistic Neural Network: argmax %d" % val)

        return val

    def decode_symbols(self, m, s):
        """
        Decode symbols S0...S5 into characters

        :param m:
        :param s:
        """
        self.ticks += m + s

        # normalize  dit = 0.1 dash = 0.3
        ten_dits = 5.0 * self.get_two_dits_size()

        symbol = self.get_probabilistic_neural_network_value(
            m / ten_dits,
            s / ten_dits,
        )

        # Debug information
        if bool(self.debug) and self.debug_level > 2:
            a = "Symbol S{0:<10} ticks:{1:<20} m:{2:<12} s:{3:<10} 2dit:{4:<10}".format(
                int(symbol),
                float(self.ticks),
                float(m),
                float(s),
                int(self.get_two_dits_size()),
            )
            self.viewer.write(column_2=a)

        if symbol == 0:
            self.add_char(".")
        elif symbol == 1:
            self.print_char(".")
        elif symbol == 2:
            self.print_word(".")
        elif symbol == 3:
            self.add_char("-")
        elif symbol == 4:
            self.print_char("-")
        elif symbol == 5:
            self.print_word("-")
        else:
            # not known symbol - noise?
            sys.stdout.write("")

    def process(
        self,
        filename,
    ):
        """
        Process audio file by demodulator and envelope detector

        :param filename: Filename to a wav file
        :type filename: str
        """
        # get Wave information and inform MorseDecoder about it
        # Set the path
        self.set_wave_path(filename)
        # Store the wave object
        self.set_wave(wave.open(self.get_wave_path()))
        # Set the format
        self.format = pyaudio.get_format_from_width(self.get_wave().getsampwidth())
        # Set the channel number
        self.channels = self.get_wave().getnchannels()
        # Set the Frame Rate
        self.rate = self.get_wave().getframerate()

        self.get_wave_informations(filename)

        # Extract Raw Audio from Wav File
        data = self.get_wave().readframes(-1)
        data = np.fromstring(
            data,
            self.get_dtype(),
        )

        # Close way we have finish with it
        self.get_wave().close()

        # this could be estimated using FFT and peak detect
        # now just assume constant CW frequency

        # demodulate audio signal with known CW frequency
        t = np.arange(len(data)) / float(self.get_rate())
        y = data * ((1 + np.sin(2 * np.pi * self.frequency * t)) / 2)

        # calculate envelope and low pass filter this demodulated signal
        # filter bandwidth impacts decoding accuracy significantly
        # for high SNR signals 50 Hz is better, for low SNR 20Hz is better
        # 25Hz is a compromise - could this be made an adaptive value?
        wn = 40.0 / (self.get_rate() / 2.0)  # 25 Hz cut-off for lowpass

        # polynomials of the IIR filter
        # (`b`) Numerator , Type: ndarray
        # (`a`) Denominator, Type ndarray
        (b, a,) = signal.butter(
            2, wn
        )  # 2nd order butter filter
        z = signal.filtfilt(
            b,
            a,
            abs(y),
        )

        # pass envelope magnitude to decoder
        # self.set_twodits(2 * self.get_dit_size() * (self.get_rate() / 1000) / self.get_wpm_default())
        # self.set_auto_gain_control_queue()
        # self.get_auto_gain_control_rolling(self.get_twodits())
        self.decode_stream(z)
        return self.get_what_i_display()

    def update_tracking(
        self,
        dit,
        dash,
    ):
        """
        Update speed tracking from (dit,dash) pair over rolling average

        :param dit:
        :param dash:
        """
        if self.dit_low_limit < dit < self.dit_high_limit:
            # Debug section
            if self.get_debug_level() > 0:
                self.viewer.write(
                    column_2="Update Tracking dit:%f dash:%f"
                    % (
                        dit,
                        dash,
                    )
                )

            # Update two_dits_size
            self.set_two_dits_size(self.get_auto_gain_control_rolling((dash + dit) / 2.0))

        if 3 * self.dit_low_limit < dash < 3 * self.dit_high_limit:
            # Debug section
            if self.get_debug_level() > 0:
                self.viewer.write(
                    column_2="Update Tracking dit:%f dash:%f"
                    % (
                        dit,
                        dash,
                    )
                )
            # Update two_dits_size
            self.set_two_dits_size(self.get_auto_gain_control_rolling((dash + dit) / 2.0))

    def edge_recorder(
        self,
        v,
        upper,
        lower,
    ):
        """
        Detect KEYDOWN/KEYUP edges, measure timing and decode symbols

        :param v:
        :param upper: Volume Upper value
        :type upper: int or float
        :param lower: Volume Lower value
        :type lower: int or float
        :return: two_dits_size value
        :rtype: int or float
        """
        keyup = 1
        keydown = 2

        if v > upper:
            if self.last == keyup:
                # calculate speed when received dit-dah  or dah-dit sequence
                if self.lastmark > 2 * self.mark:
                    # Debug section
                    if self.get_debug() and self.get_debug_level() > 1:
                        self.viewer.write(
                            column_2="Edge Recorder: update1: %f %f"
                            % (
                                self.mark,
                                self.lastmark,
                            )
                        )
                    # if verbosity:
                    #     print("update1: %f %f" % (self.mark, self.lastmark))

                    self.update_tracking(
                        self.mark,
                        self.lastmark,
                    )

                if self.mark > 2 * self.lastmark:
                    # Debug section
                    if self.get_debug() and self.get_debug_level() > 1:
                        self.viewer.write(
                            column_2="Edge Recorder: update2: %f %f"
                            % (
                                self.lastmark,
                                self.mark,
                            )
                        )
                    # if verbosity:
                    #     print("update2: %f %f" % (self.lastmark, self.mark))

                    self.update_tracking(
                        self.lastmark,
                        self.mark,
                    )

                # decode received "mark-space" symbol
                self.decode_symbols(
                    self.mark,
                    self.space,
                )
                self.lastmark = self.mark
                self.mark = 0
                self.space = 0
                self.last = keydown
                return self.get_two_dits_size()

            self.mark += 1

        elif v < lower:
            self.last = keyup
            self.space += 1

        return self.get_two_dits_size()

    def decode_stream(self, data):
        """
        decode signal envelope into MorseEncoder symbols and then characters

        :param data: signal to decode
        :type data: numpy.array
        """
        # assume 10ms signal rise time
        bfv = self.get_rate() * 0.010

        # moving average filter to smooth signal envelope - reduce noise spikes
        env = np.resize(
            np.convolve(
                data,
                np.ones(int(bfv)) / bfv,
            ),
            len(data),
        )
        mx = np.nanmax(env)
        mn = np.nanmin(env)
        mean = np.mean(env)

        # prepare arrays to collect plotting data
        agcv = np.arange(len(env))
        twodits = np.arange(len(env))
        # t = np.linspace(0, 1, len(env))

        agc_peak = mx
        i = 0
        self.set_what_i_display()
        if self.get_verbose():
            os.system("setterm -cursor off")
        while i < len(env):

            # AGC is useful if signal has rapid amplitude variations due to fading, QSB etc.
            # In computer generated audio the amplitude is not varied
            # Parameters control attack/decay time: fast attack (5) - slow decay (700)
            if self.auto_gain_control:
                z = env[i] - mean
                if z > agc_peak:
                    agc_peak = self.get_auto_gain_control_decay(
                        agc_peak,
                        z,
                        5,
                    )
                else:
                    agc_peak = self.get_auto_gain_control_decay(
                        agc_peak,
                        z,
                        700,
                    )
                agcv[i] = agc_peak
                if agc_peak > 0:
                    z /= agc_peak
                    z = self._clamp(
                        z,
                        0.0,
                        1.0,
                    )
                up = self.get_threshold_upper()
                down = self.get_threshold_lower()
            else:
                # calculate signal threshold if no AGC is used
                z = env[i]
                up = self.get_threshold_upper() * (mx - mn)
                down = self.get_threshold_lower() * (mx - mn)

            # capture estimated speed over time for plotting
            twodits[i] = self.edge_recorder(
                z,
                up,
                down,
            )

            i += 1

            # make it slow , but work exactly as it should
            if self.get_debug():
                self.viewer.write(
                    status_symbol="<",
                    prompt=True,
                    column_1=str(self.__class__.__name__ + ":"),
                    column_2=str(self.get_what_i_display()),
                )
        if self.get_debug():
            self.viewer.flush_a_new_line()
            os.system("setterm -cursor on")

    @staticmethod
    def _clamp(x, mn, mx):
        """
        Clamps output between mn and mx values

        :param x: the value to clamp
        :param mn: min
        :param mx: max
        :return: clamped value
        """
        if x > mx:
            return mx
        if x < mn:
            return mn
        else:
            return x


# def main(*args, **kwargs):
#     global debug
#     global debug_level
#     global auto_gain_control
#
#     parser = OptionParser(usage="%prog [OPTIONS] <audio files>\nDecodes morse_encoder code from Wave audio files")
#
#     parser.add_option("-v", "--debug",
#                       action="count",
#                       dest="debug",
#                       default=False,
#                       help="prints details about errors and calls. -vv -vvv for increase the debugging level."
#                       )
#     parser.add_option("-a", "--auto_gain",
#                       action="store_true",
#                       dest="agv",
#                       default=False,
#                       help="use automatic gain control"
#                       )
#
#     (options, args) = parser.parse_args()
#     if options.verbose:
#         debug = True
#         debug_level = options.verbose
#     if options.agv:
#         auto_gain_control = True
#     if len(args) < 1:
#         sys.stdout.write('usage: [OPTIONS] <audio files>')
#         sys.stdout.write('\n')
#         sys.stdout.flush()
#         sys.exit(1)
#
#     # Process all audio files given as arguments
#     for i in range(0, len(args)):
#         # We are so gentle
#         with MorseDecoder() as morse_decoder:
#             try:
#                 # Check for options
#                 if options.verbose:
#                     morse_decoder.set_debug(debug)
#                     morse_decoder.set_debug_level(debug_level)
#                 if options.agv:
#                     morse_decoder.auto_gain_control = True
#
#                 # Start to do something
#                 morse_decoder.process(args[i])
#
#                 # Add a new line at end
#                 sys.stdout.write('\n')
#                 sys.stdout.flush()
#
#             # Yes exit normally is possible
#             except KeyboardInterrupt:
#                 sys.exit(0)
#
#
# if __name__ == "__main__":
#     main()
