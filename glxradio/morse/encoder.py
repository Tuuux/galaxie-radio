#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

# Inspired by: http://f2qh.fr/morse_200514_v1/textes_clair.html
# Inspired by: https://github.com/ag1le/morse.py/blob/master/spectrogram.py
# Inspired by: https://github.com/ag1le/morse.py
# Inspired by: http://f2qh.fr/morse_200514_v1/qso.html
# Inspired by: Decode : https://stackoverflow.com/questions/43012715/morse-audio-deco
# Inspired by: https://stackoverflow.com/questions/4160175/detect-tap-with-pyaudio-from-live-mic
# Inspired by: https://github.com/cardre/morsewav.py/blob/master/morsewav.py
# Inspired by: http://f2qh.fr/morse_200514_v1/generalites.html
# Inspired by: http://oz123.github.io/writings/morse-fun-with-python/index.html
# Inspired by: https://github.com/morse-talk/morse-talk

import os
import unicodedata
import wave
import math
import time
import array

from glxviewer import Viewer
from glxaudio.AudioConstants import GLXAUDIO
from glxradio.morse.code import MorseCode
from glxradio.libs.frequency import Frequency
from glxaudio.Audio import Audio
import warnings

warnings.filterwarnings("ignore", category=FutureWarning)


class MorseEncoder(MorseCode, Frequency, Audio):
    def __init__(self, dit_sign=".", dah_sign="-"):
        MorseCode.__init__(self)
        Frequency.__init__(self)
        Audio.__init__(self)

        self.dit_sign = None
        self.dah_sign = None

        # Set information for the first time
        self.debug = True
        self.verbose = True
        self.format = GLXAUDIO.FORMAT_INT16
        self.channels = 1
        self.rate = 8000

        self.get_dah_sign
        # Audio Frequency (Hz)
        # 500
        # 550
        # 600
        # 650
        # 700
        # 750
        # 800
        # 850
        # 900
        # 950
        # 1000

        # self.set_frequency(600)
        self.set_dit_sign(dit_sign)
        self.set_dah_sign(dah_sign)

        # MorseEncoder Code Speed (words / minute)
        # 5WPM
        # 7WPM
        # 10WPM
        # 13WPM
        # 15WPM
        # 17WPM
        # 20WPM
        # 25WPM
        # 30WPM
        self.dot_dur = 1.20 / 25  # 50 ms 5WPM=0.240 1.43/WPM
        self.volume = 80  # 80%
        self.DOT_DURATION = 1
        self.DAH_DURATION = 3 * self.DOT_DURATION
        self.INTER_GAP = self.DOT_DURATION
        self.LETTERS_GAP = 3 * self.DOT_DURATION
        self.WORDS_GAP = 7 * self.DOT_DURATION

        # International support
        self.use_international_code = True

        self.viewer = Viewer()

    def __enter__(self, dit_sign=".", dah_sign="-"):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def set_dit_sign(self, sign):
        """
        Set the dit sign to use. All MorseEncoder() function use it.

        :param sign: the dit sign to use
        :type sign: str
        """
        # Exit as soon of possible
        if type(sign) != str:
            raise TypeError("'sign' must be a str type")

        # make the job in case
        if self.get_dit_sign() != sign:
            self.dit_sign = sign

    def get_dit_sign(self):
        """
        Return the dit sign as set by MorseEncoder().set_sign()

        :return: dit sign
        :rtype: str
        """
        return self.dit_sign

    def set_dah_sign(self, sign):
        """
        Set the dah sign to use. All MorseEncoder() function use it.

        :param sign: the dah sign to use
        :type sign: str
        """
        # Exit as soon of possible
        if type(sign) != str:
            raise TypeError("'sign' must be a str type")

        # make the job in case
        if self.get_dah_sign() != sign:
            self.dah_sign = sign

    def get_dah_sign(self):
        """
        Return the dit sign as set by MorseEncoder().set_dah_sign()

        :return: dah_sign
        :rtype: str
        """
        return self.dah_sign

    def encode(self, text):
        """
        Translate a string to morse_encoder code in dot dash form

        :param text: text to convert to morse_encoder code
        :type text: str
        :return: MorseEncoder encoded string
        :rtype: str
        :raise TypeError: when ``text`` is not a str type
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        # no luck to you if you send a un-allowed char
        for char in text:
            if char not in self.table and char != " ":
                raise TypeError("char not in morse_code_table")

        # we make the job
        result = ""

        for c in text:
            result += self.table.get(c, c) + " "

        # cut the last space and replace dits, dahs
        result = result[:-1].replace(".", self.get_dit_sign()).replace("-", self.get_dah_sign())

        # return something just i case
        return result

    def decode(self, text):
        """
        Translate dash dot morse_encoder code to ascii text

        :param text: text to convert to morse_encoder code
        :type text: str
        :return: ascii text converted from morse_encoder code
        :rtype: str
        :raise TypeError: when ``text`` is not a str type
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a array type")

        decode_morse_table = dict((self.table[k], k.upper()) for k in self.table)
        result = ""
        words = text.split("   ")

        for w in words:
            for c in w.split(" "):
                result += decode_morse_table.get(c, c)
            result += " "

        # cut the last space
        result = result[:-1]

        # return something because that cool to return something ;)
        return result

    def encode_exact(self, text):
        """
        Encode with exact time conventions

        :param text: text to convert to morse_encoder code
        :type text: str
        :return: ascii text converted from morse_encoder code
        :rtype: str
        :raise TypeError: when ``text`` is not a str type
        :raise TypeError: when on char of ``text`` is not on morse_code_table
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a array type")

        # no luck to you if you send a un-allowed char
        for char in text:
            if char not in self.table and char != " ":
                raise TypeError("char not in morse_code_table")

        # we start
        result = ""

        for c in text:
            result += (" " * self.INTER_GAP).join(list(self.table.get(c, c))) + " " * self.LETTERS_GAP

        # cut the last spaces and replace dits, dahs
        result = result[: -self.LETTERS_GAP].replace(".", self.get_dit_sign()).replace("-", self.get_dah_sign())

        # Not sure about it help a lot
        # result = " " + result

        return result

    def text_to_morse_for_human(self, text, separator=" / "):
        """
        Translate a string to morse_encoder code in dot dash form. For human.

        :param text: text to convert to morse_encoder code
        :type text: str
        :param separator: the separator to use for word separator
        :type separator: str
        :return: a morse_encoder code it use ``separator`` as word separator
        :rtype: str
        :raise TypeError: when ``separator`` is not a str type
        :raise TypeError: when ``text`` is not a str type
        :raise TypeError: when on char of ``text`` is not on morse_code_table
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        # no luck to you if you send a un-allowed char
        for char in text:
            if char not in self.table and char != " ":
                raise TypeError("char not in morse_code_table")

        if type(separator) != str:
            raise TypeError("'separator' must be a str type")

        morse = []
        for word in text.split():
            morse_word = []
            for w in word:
                morse_word.append(self.table[w])

            morse.append(" ".join(morse_word))

        return separator.join(morse)

    def morse_to_text_for_human(self, text, separator=" / "):
        """
        Translate dash dot morse_encoder code to ascii text, for human

        :param text: text to convert to morse_encoder code
        :type text: str
        :param separator: the separator to use for word separator
        :type separator: str
        :return: ascii text converted from morse_encoder code
        :rtype: str
        :raise TypeError: when ``separator`` is not a str type
        :raise TypeError: when ``text`` is not a str type
        :raise TypeError: when on char of ``text`` is not on morse_code_table
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        if type(separator) != str:
            raise TypeError("'separator' must be a str type")

        translated = []
        words = text.split(separator)
        for word in words:
            clear = []
            chars = word.split()
            for c in chars:
                clear.append(self.table_reversed[c].upper())
            translated.append("".join(clear))

        return " ".join(translated)

    def text_to_morse_wave(self, text, filename):
        """
        Convert a morse_encoder message, to a wav audio file.

        If text=None then the temporary wav file will not be delete.

        Care about communication should stay anonyme.

        :param text: the morse_encoder message to convert, as return by MorseEncoder.text_to_morse()
        :type text: str
        :param filename: a wave_path where save the wav file or None
        :return: a wave_path
        :rtype: str
        :raise TypeError: when ``text`` is not a str type
        :raise TypeError: when ``filename`` argument is not a :py:data:`string`
        :raise IOError: when ``filename`` does not exist
        :raise IOError: when ``filename`` is not file
        :raise IOError: when ``filename`` can't be read
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        if self.format == 0:
            pass

        # no luck to you if you send a un-allowed char
        for char in text:
            if char not in self.table and char != " ":
                raise TypeError("char not in morse_code_table")

        # Exit as soon of possible
        if type(filename) != str:
            raise TypeError("'filename' must be a str type")

        if not os.path.exists(os.path.realpath(filename)):
            raise FileNotFoundError("'filename' does not exist")

        if not os.access(os.path.realpath(filename), os.R_OK):
            raise IOError("'%s' can't be read" % filename)

        if not os.path.isfile(os.path.realpath(filename)):
            raise IOError("'filename' must be a file")

        encoded_exact_text = MorseEncoder("=", "===").encode_exact(text)

        duration_start = time.time()

        if self.get_debug():
            self.viewer.write(column_1=self.__class__.__name__ + str(":"), column_2="save file")

        self.set_wave_path(filename)
        self.set_wave(wave.open(self.get_wave_path(), "wb"))

        self.get_wave().setnchannels(self.get_channels())
        self.get_wave().setframerate(self.get_rate())
        # 2 bytes because of using signed short integers => bit depth = 16
        self.get_wave().setsampwidth(2)

        # signed short integer (-32768 to 32767) data
        data = array.array("h")

        period = self.get_rate() / self.frequency

        # Append a silence
        for i in range(int(self.get_rate() * self.dot_dur)):
            data.append(int(0))

        for c in encoded_exact_text:
            if c != " ":
                for i in range(int(self.get_rate() * self.dot_dur)):
                    sample = self.get_base_ten_signed_max_value() * float(self.volume) / 100
                    sample *= math.sin(math.pi * 2 * (i % period) / period)
                    data.append(int(sample))
            else:
                for i in range(int(self.get_rate() * self.dot_dur)):
                    sample = self.get_base_ten_signed_max_value() * float(0.0) / 100
                    sample *= math.sin(math.pi * 2 * (i % period) / period)
                    data.append(int(sample))

        # Append a silence
        for i in range(int((self.get_rate() * self.dot_dur) * 2)):
            data.append(int(0))
        for i in range(int(self.get_rate() * self.dot_dur)):
            sample = self.get_base_ten_signed_max_value() * float(self.volume) / 100
            sample *= math.sin(math.pi * 2 * (i % period) / period)
            data.append(int(sample))
        for i in range(int((self.get_rate() * self.dot_dur))):
            data.append(int(0))

        self.get_wave().writeframes(data.tobytes())

        # Debug information's
        if self.get_debug():
            self.get_wave_informations(self.get_wave_path())

        # Close everything
        # self.close()

        if self.get_verbose():
            self.viewer.write(
                status_text="MORSE",
                status_text_color="WHITE",
                column_1="Create {processed_elements} element(s) in {time_it_have_take} s".format(
                    processed_elements=int(round(self.get_message_length(self.decode(text)))),
                    time_it_have_take=round((time.time() - duration_start), ndigits=3),
                ),
            )

        return filename

    def get_message_length(self, text, n=1, word_spaced=True):
        """
        Translate dash dot morse_encoder code to ascii text, for human

        :param text: text to get the lenght
        :type text: str
        :param n: how mush a char cost, normally that 1
        :type n: int
        :param word_spaced: True if we consider a space at the end of the message, then PARIS = 50
        :type word_spaced: bool
        :return: the length of the ``text``
        :rtype: int
        :raise TypeError: when ``text`` is not a str type
        :raise TypeError: when ``n`` is not a int type
        :raise TypeError: when ``word_spaced`` is not a bool type
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        if type(n) != int:
            raise TypeError("'n' must be a int type")

        if type(word_spaced) != bool:
            raise TypeError("'word_spaced' must be a bool type")

        # we start
        text = str(text)
        text = text * n

        if word_spaced:
            text = text + " E"
        lst_bin = self.text_to_bin(text)
        n = len(lst_bin)
        if word_spaced:
            n -= 1  # E is one "dit" so we remove it

        # that time to return something
        return n

    def text_to_bin(self, text):
        """
        Translate a ascii text to MorseEncoder with a binary representation

        :param text: text to convert to morse_encoder code
        :type text: str
        :return: ascii text converted from morse_encoder code with 0 or 1
        :rtype: str
        :raise TypeError: when ``text`` is not a str type
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        # we make the job
        morse = []

        for word in text.split():
            morse_word = []

            for w in word:
                morse_char = []

                for char in self.table[w]:
                    if char == ".":
                        morse_char.append("1")
                    if char == "-":
                        morse_char.append("111")

                morse_word.append("0".join(morse_char))

            morse.append("000".join(morse_word))

        return "0000000".join(morse)

    @staticmethod
    def normalize_text(text):
        """
        Function it permit to clean the text before encoding to morse_encoder.

        :param text: the text to clean
        :param text: str or unicode
        :return: the text cleaned
        :rtype: str
        :raise TypeError: when ``text`` is not a str type
        """
        # exit as soon of possible
        if type(text) != str:
            raise TypeError("'text' must be a str type")

        return bytes.decode(unicodedata.normalize("NFKD", text).encode("ASCII", "ignore"))
