#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved


class MorseCode(object):
    def __init__(self):
        # https://morsecode.world/international/morse2.html
        # https://www.itu.int/dms_pubrec/itu-r/rec/m/R-REC-M.1172-0-199510-I!!PDF-E.pdf
        self.__use_international_code = None
        self.__table_normal = None
        self.__table_special = None

        # Init for the first time
        self.table = None
        self.table_special = None
        self.use_international = None

    @property
    def table(self):
        return self.__table_normal

    @table.setter
    def table(self, value=None):
        if value is None:
            value = {
                "!": "-.-.--",
                '"': ".-..-.",
                "$": "...-..-",
                "&": ".-...",
                "'": ".----.",
                "(": "-.--.",
                ")": "-.--.-",
                "+": ".-.-.",
                ",": "--..--",
                "-": "-....-",
                ".": ".-.-.-",
                "/": "-..-.",
                "0": "-----",
                "1": ".----",
                "2": "..---",
                "3": "...--",
                "4": "....-",
                "5": ".....",
                "6": "-....",
                "7": "--...",
                "8": "---..",
                "9": "----.",
                ":": "---...",
                ";": "-.-.-.",
                "=": "-...-",
                "?": "..--..",
                "@": ".--.-.",
                "A": ".-",
                "B": "-...",
                "C": "-.-.",
                "D": "-..",
                "E": ".",
                "F": "..-.",
                "G": "--.",
                "H": "....",
                "I": "..",
                "J": ".---",
                "K": "-.-",
                "L": ".-..",
                "M": "--",
                "N": "-.",
                "O": "---",
                "P": ".--.",
                "Q": "--.-",
                "R": ".-.",
                "S": "...",
                "T": "-",
                "U": "..-",
                "V": "...-",
                "W": ".--",
                "X": "-..-",
                "Y": "-.--",
                "Z": "--..",
                "_": "..--.-",
                "a": ".-",
                "b": "-...",
                "c": "-.-.",
                "d": "-..",
                "e": ".",
                "f": "..-.",
                "g": "--.",
                "h": "....",
                "i": "..",
                "j": ".---",
                "k": "-.-",
                "l": ".-..",
                "m": "--",
                "n": "-.",
                "o": "---",
                "p": ".--.",
                "q": "--.-",
                "r": ".-.",
                "s": "...",
                "t": "-",
                "u": "..-",
                "v": "...-",
                "w": ".--",
                "x": "-..-",
                "y": "-.--",
                "z": "--..",
            }
        if type(value) != dict:
            raise TypeError("'morse_code_table' property value must be a dict type or None")
        if self.table != value:
            self.__table_normal = value

    @property
    def table_special(self):
        return self.__table_special

    @table_special.setter
    def table_special(self, value=None):
        if value is None:
            value = {
                "<AA>": ".-.-",  # New line
                "<AR>": ".-.-.",  # End of message (also +)
                "<AS>": ".-...",  # Wait
                "<BK>": "-...-.-",  # Break
                "<BT>": "-...-",  # New paragraph (also =)
                "<CL>": "-.-..-..",  # Going off the air ("clear")
                "<CT>": "-.-.-",  # Start copying
                "<DO>": "-..---",  # Change to wabun code
                "<KA>": "-.-.-",  # Starting signal
                "<KN>": "-.--.",  # Invite a specific station to transmit
                "<SK>": "...-.-",  # End of transmission (also <VA>)
                "<SN>": "...-.",  # Understood (also <VE>)
                "<SOS>": "...---...",  # Distress message
                "<VA>": "...-.-",  # End of transmission
                "<VE>": "...-.",  # Understood
                "<HH>": "........",
                "<IMI>": "..--.",
                "<LL>": ".-...-..",
                "<PP>": ".--..--.",
                "<RF>": ".-...-.",
                "<SL>": "....-..",
            }
        if type(value) != dict:
            raise TypeError("'morse_code_table_special' property value must be a dict type or None")
        if self.table_special != value:
            self.__table_special = value

    @property
    def table_reversed(self):
        return {morse: letter.upper() for letter, morse in self.table.items()}

    @property
    def use_international(self):
        """
        Return True is we use International code

        :return: True for use international MorseEncoder Code
        :rtype: bool
        """
        return self.__use_international_code

    @use_international.setter
    def use_international(self, value=True):
        """
        A trigger for know if we us ethe International Code

        Default is True

        :param value: True for use international MorseEncoder Code
        :type value: bool
        """
        if value is None:
            value = True
        if type(value) != bool:
            raise TypeError("'value' must be a bool type")
        if self.use_international != value:
            self.__use_international_code = value
        if self.use_international:
            self.table["Ä"] = ".-.-"
            self.table["ä"] = ".-.-"
            self.table["Æ"] = ".-.-"
            self.table["æ"] = ".-.-"
            self.table["À"] = ".--.-"
            self.table["à"] = ".--.-"
            self.table["Å"] = ".--.-"
            self.table["å"] = ".--.-"
            self.table["Ĉ"] = "-.-.."
            self.table["ĉ"] = "-.-.."
            self.table["Ç"] = "-.-.."
            self.table["ç"] = "-.-.."
            self.table["CH"] = "----"
            self.table["ch"] = "----"
            self.table["Ð"] = "..--."
            self.table["ð"] = "..--."
            self.table["È"] = ".-..-"
            self.table["è"] = ".-..-"
            self.table["É"] = "..-.."
            self.table["é"] = "..-.."
            self.table["Ĝ"] = "--.-."
            self.table["ĝ"] = "--.-."
            self.table["Ĥ"] = "-.--."
            self.table["ĥ"] = "-.--."
            self.table["Ĵ"] = ".---."
            self.table["ĵ"] = ".---."
            self.table["Ñ"] = "--.--"
            self.table["ñ"] = "--.--"
            self.table["Ö"] = "-.-.."
            self.table["ö"] = "-.-.."
            self.table["Ø"] = "---."
            self.table["ø"] = "---."
            self.table["Ŝ"] = "...-."
            self.table["ŝ"] = "...-."
            self.table["þ"] = ".--.."
            self.table["Þ"] = ".--.."
            self.table["Ü"] = "..--"
            self.table["ü"] = "..--"
            self.table["Ŭ"] = "..--"
            self.table["ŭ"] = "..--"
        else:
            char_to_delete = [
                "Ä",
                "ä",
                "Æ",
                "æ",
                "À",
                "à",
                "Å",
                "å",
                "Ĉ",
                "ĉ",
                "Ç",
                "ç",
                "CH",
                "ch",
                "Ð",
                "ð",
                "È",
                "è",
                "É",
                "é",
                "Ĝ",
                "ĝ",
                "Ĥ",
                "ĥ",
                "Ĵ",
                "ĵ",
                "Ñ",
                "ñ",
                "Ö",
                "ö",
                "Ø",
                "ø",
                "Ŝ",
                "ŝ",
                "þ",
                "Þ",
                "Ü",
                "ü",
                "Ŭ",
                "ŭ",
            ]
            for char in char_to_delete:
                try:
                    del self.table[char]
                except KeyError:
                    pass
        # Create the reverse dict for morse_encoder to text
        self.morse_code_table_reversed = {morse: letter.upper() for letter, morse in self.table.items()}
