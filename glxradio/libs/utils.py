#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import term


def sec2time(sec, n_msec=3):
    """
    Convert seconds to 'D days, HH:MM:SS.FFF'

    By: Lee
     Source: https://stackoverflow.com/questions/775049/how-do-i-convert-seconds-to-hours-minutes-and-seconds

    Example:
     $ sec2time(10, 3)
     Out: '00:00:10.000'

     $ sec2time(1234567.8910, 0)
     Out: '14 days, 06:56:07'

     $ sec2time(1234567.8910, 4)
     Out: '14 days, 06:56:07.8910'

     $ sec2time([12, 345678.9], 3)
     Out: ['00:00:12.000', '4 days, 00:01:18.900']

    :param sec:
    :param n_msec:
    :return:
    """
    if hasattr(sec, '__len__'):
        return [sec2time(s) for s in sec]
    m, s = divmod(sec, 60)
    h, m = divmod(m, 60)
    d, h = divmod(h, 24)
    if n_msec > 0:
        pattern = '%%02d:%%02d:%%0%d.%df' % (n_msec + 3, n_msec)
    else:
        pattern = r'%02d:%02d:%02d'
    if d == 0:
        return pattern % (h, m, s)
    return ('%d days, ' + pattern) % (d, h, m, s)


def resize_text(text='', max_width=0, separator='~'):
    """
    Resize the text , and return a new text

    example: return '123~789' for '123456789' where max_width = 7 or 8

    :param text: the original text to resize
    :type text: str
    :param max_width: the size of the text
    :type max_width: int
    :param separator: a separator a in middle of the resize text
    :type separator: str
    :return: a resize text
    :rtype: str
    """
    # Try to quit as soon of possible
    if type(text) != str:
        raise TypeError('"text" must be a str type')
    if type(max_width) != int:
        raise TypeError('"max_width" must be a int type')
    if type(separator) != str:
        raise TypeError('"separator" must be a str type')

    # If we are here we haven't quit
    if max_width < len(text):
        if max_width <= 0:
            return str('')
        elif max_width == 1:
            return str(text[:1])
        elif max_width == 2:
            return str(text[:1] + text[-1:])
        elif max_width == 3:
            return str(text[:1] + separator[:1] + text[-1:])
        else:
            max_width -= len(separator[:1])
            max_div = int(max_width / 2)
            return str(text[:max_div] + separator[:1] + text[-max_div:])
    else:
        return str(text)


def center_text(text='', max_width=None):
    """
    Return a string where
    :param text:
    :param max_width:
    :return:
    """
    if max_width is None:
        _, max_width = term.getSize()

    string_to_return = ''

    space_prepend_size = int((max_width / 2) - (len(text) / 2))
    space_prepend_str = ' ' * space_prepend_size

    string_to_return += space_prepend_str
    string_to_return += text

    space_append_size = int(max_width - len(string_to_return))
    space_append_str = ' ' * space_append_size

    string_to_return += space_append_str

    return string_to_return


def bracket_text(text=''):
    string_to_return = ''
    string_to_return += '['
    string_to_return += text
    string_to_return += ']'

    return string_to_return
