#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

class Frequency(object):

    def __init__(self):
        self.__frequency = None

        # Init for the first time
        self.frequency = None

    @property
    def frequency(self):
        """
        Return the frequency of the tone as set by Morse.set_frequency()

        :return: The frequency
        :rtype: float
        """
        return self.__frequency

    @frequency.setter
    def frequency(self, value):
        """
        Set the frequency of the tone.

        Default: 1240.0

        :param value: cycles per second (Hz) (frequency of the sine waves)
        :type value: int or float
        """
        if value is None:
            value = 600.0
        if type(value) != int and type(value) != float:
            raise TypeError("'frequency' property value must be a int or a float type")
        if self.frequency != float(value):
            self.__frequency = float(value)
