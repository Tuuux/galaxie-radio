from argparse import ArgumentParser

parser_frequency = ArgumentParser(
    prog='frequency',
    description='change the frequency tone',
    add_help=True,
)

parser_frequency.add_argument(
    "frequency",
    nargs="?",
    const=0,
    help="a int or float value in Hertz, default is 600",
)