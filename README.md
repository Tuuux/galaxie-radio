[![Documentation Status](https://readthedocs.org/projects/galaxie-radio/badge/?version=latest)](http://galaxie-radio.readthedocs.io/?badge=latest)
[![pipeline status](https://gitlab.com/Tuuux/galaxie-radio/badges/master/pipeline.svg)](https://gitlab.com/Tuuux/galaxie-radio/commits/master)
[![codecov](https://codecov.io/gl/Tuuux/galaxie-radio/branch/master/graph/badge.svg)](https://codecov.io/gl/Tuuux/galaxie-radio)

Galaxie Radio Documentation
============================
<div style="text-align:center"><img src ="https://gitlab.com/Tuuux/galaxie-radio/raw/master/docs/source/images/logo_galaxie.png" /></div>

Actual Homepage : https://gitlab.com/Tuuux/galaxie-radio

Documentation : https://galaxie-radio.readthedocs.io

The Project
-----------
**Galaxie Radio** is a free software ToolKit for Amateur Radio.

Originally the project have start in 2018 when the author Jérôme.O Alias Tuuux have start to learn Radio Communication.

The Mission
-----------
Provide a Text Based ToolKit for assist Amateur Radio.

MorseEncoder Decoder/Decoder Text and Audio fully tested and documented API. What else ?

Example of a Simplex Repeater fully functional
----------------------------------------------

```python

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import sys
import os
import tempfile

# Require when you haven't GLXRadio as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))

from GLXRadio.AudioRecorder import AudioRecorder
from GLXRadio.AudioPlayer import AudioPLayer
from GLXRadio.AudioInterfaces import AudioInterfaces
from GLXRadio.Sleep import Sleep
from GLXRadio.Constants import GLXR
from GLXRadio.Viewer import flush_infos

# THE APP
try:
    verbose = True
    debug = True
    debug_level = 2

    time_to_sleep = 0.42

    if verbose:
        flush_infos(
            status_text='INIT',
            status_text_color='WHITE',
            column_1='Simplex Repeater',
            column_2=' - Version 0.5'
        )

        flush_infos(
            status_text='INIT',
            status_text_color='WHITE',
            column_1=AudioInterfaces.__name__ + ':',
            column_2='list interfaces'
        )
        AudioInterfaces.print_interfaces(only_default=True)

    while True:
        #  Create a new temporary file each time, that because communication's should be anonyme
        temporary_file = tempfile.NamedTemporaryFile()
        try:
            # Start a recording
            with AudioRecorder() as recorder:
                recorder.set_debug(debug)
                recorder.set_debug_level(debug_level)
                recorder.set_verbose(verbose)
                recorder.set_format(GLXR.AUDIO_FORMAT_INT16)
                recorder.set_threshold(5)  # in percent
                recorder.set_channels(1)
                recorder.set_rate(22050)
                recorder.set_frames_per_buffer(1024)
                recorder.record(filename=temporary_file.name)

            # Wait , because that is how work a repeater
            with Sleep() as sleeper:
                sleeper.set_debug(debug)
                sleeper.set_debug_level(debug_level)
                sleeper.set_verbose(verbose)
                sleeper.sleep(time_to_sleep)

            # Play what is inside our temporary file
            with AudioPLayer() as player:
                player.set_debug(debug)
                player.set_debug_level(debug_level)
                player.set_verbose(verbose)
                player.play(filename=temporary_file.name)

        except EOFError:
            pass

        # Close the temporary file, it have effect to delete the file, that because communication's should be anonyme
        temporary_file.close()

except KeyboardInterrupt:
    sys.exit(0)
```

Output
------
<div style="text-align:center"><img src ="https://gitlab.com/Tuuux/galaxie-radio/raw/master/docs/source/images/simplex_repeater_debug.png" /></div>

More examples can be found here: https://gitlab.com/Tuuux/galaxie-radio/tree/master/exemple

Spectrograpm it's possible with UTF-8 char
<div style="text-align:center"><img src ="https://gitlab.com/Tuuux/galaxie-radio/raw/master/docs/source/images/spectrogram01.png" /></div>

<div style="text-align:center"><img src ="https://gitlab.com/Tuuux/galaxie-radio/raw/master/docs/source/images/spectrogram01.png" /></div>


Features
--------
* Generate CSV file with PMR446 thing inside
* A Text MorseEncoder coder decoder
* A Audio MorseEncoder coder AND decoder (yes decoder) from the version of MorseEncoder wave decode of Mauri Niininen, AG1LE.
* A wave player
* A wave recoder with Noise Gate, Attack, Release, Normalization and Trim
* A Viewer it display thing via a format, no strange code
* Better i can i'll test code with unitest
* A spectrogram text based , fully usable it use UTF-8 char for better result.
* Ready to learn MorseEncoder (May be soon a Text user Interface)


Contribute
----------

- Issue Tracker: https://gitlab.com/Tuuux/galaxie-radio/issues
- Source Code: https://gitlab.com/Tuuux/galaxie-radio

Our collaboration model is the Collective Code Construction Contract (C4): https://rfc.zeromq.org/spec:22/C4/