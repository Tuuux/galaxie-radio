#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxradio.libs.utils import sec2time
from glxradio.libs.utils import resize_text
from glxradio.libs.utils import center_text
from glxradio.libs.utils import bracket_text


import unittest


# Unittest
class TestUtils(unittest.TestCase):
    # Test

    def test_sec2time(self):
        """Test sec2time()"""
        self.assertEqual(sec2time(10, 3), '00:00:10.000')
        self.assertEqual(sec2time(1234567.8910, 0), '14 days, 06:56:07')
        self.assertEqual(sec2time(1234567.8910, 4), '14 days, 06:56:07.8910')
        self.assertEqual(sec2time([12, 345678.9], 3), ['00:00:12.000', '4 days, 00:01:18.900'])

    def test_center_text(self):
        """Test View.center_text()"""
        self.assertEqual('  *  ', center_text(text='*', max_width=5))
        self.assertEqual(' TX  ', center_text(text='TX', max_width=5))
        self.assertEqual(' RX  ', center_text(text='RX', max_width=5))
        self.assertEqual(' DLA ', center_text(text='DLA', max_width=5))
        self.assertEqual('INIT ', center_text(text='INIT', max_width=5))
        self.assertEqual('DEBUG', center_text(text='DEBUG', max_width=5))

        self.assertEqual(str, type(center_text(text='*', max_width=None)))
        self.assertGreater(len(center_text(text='*', max_width=None)), 0)

    def test_resize_text(self):
        """Test Utils.clamp_to_zero()"""
        text = "123456789"
        width = 10
        self.assertEqual(text, resize_text(text, width, '~'))
        width = 9
        self.assertEqual(text, resize_text(text, width, '~'))
        width = 8
        self.assertEqual('123~789', resize_text(text, width, '~'))
        width = 7
        self.assertEqual('123~789', resize_text(text, width, '~'))
        width = 6
        self.assertEqual('12~89', resize_text(text, width, '~'))
        width = 5
        self.assertEqual('12~89', resize_text(text, width, '~'))
        width = 4
        self.assertEqual('1~9', resize_text(text, width, '~'))
        width = 3
        self.assertEqual('1~9', resize_text(text, width, '~'))
        width = 2
        self.assertEqual('19', resize_text(text, width, '~'))
        width = 1
        self.assertEqual('1', resize_text(text, width, '~'))
        width = 0
        self.assertEqual('', resize_text(text, width, '~'))
        width = -1
        self.assertEqual('', resize_text(text, width, '~'))

        # Test Error
        self.assertRaises(TypeError, resize_text, text=text, max_width=width, separator=int(42))
        self.assertRaises(TypeError, resize_text, text=text, max_width='coucou', separator='~')
        self.assertRaises(TypeError, resize_text, text=int(42), max_width=width, separator='~')

    def test_bracket_text(self):
        """Test bracket_text()"""
        self.assertEqual('[*]', bracket_text(text='*'))
        self.assertEqual('[TX]', bracket_text(text='TX'))
        self.assertEqual('[RX]', bracket_text(text='RX'))
        self.assertEqual('[DLA]', bracket_text(text='DLA'))
        self.assertEqual('[INIT]', bracket_text(text='INIT'))
        self.assertEqual('[DEBUG]', bracket_text(text='DEBUG'))
