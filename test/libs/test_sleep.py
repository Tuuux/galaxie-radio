#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxradio.libs.sleep import Sleep
import unittest
import time


# Unittest
class TestSleep(unittest.TestCase):
    # Test
    def test_SleepSleep(self):
        sleep = Sleep()
        sleep.verbose = True
        sleep.debug = True
        start = time.time()
        sleep.sleep(0.42)
        end = time.time()
        self.assertEqual(float("{:0.2f}".format(end - start)), 0.42)

    def test_Sleep_with(self):
        with Sleep() as sleeper:
            sleeper.debug = True
            sleeper.debug_level = 2
            sleeper.verbose = True
            sleeper.sleep(0.42)

    def test_duration_start(self):
        sleep = Sleep()

        self.assertEqual(None, sleep.duration_start)

        sleep.start()
        self.assertEqual(float, type(sleep.duration_start))
