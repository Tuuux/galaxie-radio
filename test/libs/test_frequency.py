import unittest

from glxradio.libs.frequency import Frequency


# Unittest
class TestLibsFrequency(unittest.TestCase):
    def test_frequency(self):
        """Test Morse.set_frequency()"""
        frequency_object = Frequency()

        self.assertEqual(600, frequency_object.frequency)

        frequency_object.frequency = 1200
        self.assertEqual(1200, frequency_object.frequency)

        # test error
        self.assertRaises(TypeError, setattr, frequency_object, "frequency", "Hello.42")
