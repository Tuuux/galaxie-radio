#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxradio.morse.code import MorseCode
import unittest


class TestMorseCode(unittest.TestCase):
    def setUp(self):
        self.morse = MorseCode()

    def test_use_international(self):
        char_to_test = [
            "Ä",
            "ä",
            "Æ",
            "æ",
            "À",
            "à",
            "Å",
            "å",
            "Ĉ",
            "ĉ",
            "Ç",
            "ç",
            "CH",
            "ch",
            "Ð",
            "ð",
            "È",
            "è",
            "É",
            "é",
            "Ĝ",
            "ĝ",
            "Ĥ",
            "ĥ",
            "Ĵ",
            "ĵ",
            "Ñ",
            "ñ",
            "Ö",
            "ö",
            "Ø",
            "ø",
            "Ŝ",
            "ŝ",
            "þ",
            "Þ",
            "Ü",
            "ü",
            "Ŭ",
            "ŭ",
        ]

        self.assertEqual(True, self.morse.use_international)

        self.morse.use_international = False
        self.assertEqual(False, self.morse.use_international)

        # test that is add
        self.morse.use_international = True
        len_with = len(self.morse.table)

        self.morse.use_international = False
        len_without = len(self.morse.table)

        self.assertNotEqual(len_with, len_without)

        # we back to normal value
        self.morse.use_international = True
        len_without = len(self.morse.table)
        self.assertEqual(len_with, len_without)

        for char in char_to_test:
            self.assertTrue(self.morse.table[char])

        self.morse.use_international = False
        for char in char_to_test:
            try:
                _ = self.morse.table[char]
                check = True
            except KeyError:
                check = False
                self.assertTrue(True)
            self.assertFalse(check)

        self.morse.use_international = True
        for char in char_to_test:
            try:
                _ = self.morse.table[char]
                check = True
            except KeyError:
                check = False
                self.assertTrue(True)
            self.assertTrue(check)

        # test the exception KeyError
        self.morse.use_international = True
        del self.morse.table["é"]
        self.morse.use_international = False

        # test error
        self.assertRaises(TypeError, setattr, self.morse, "use_international", "Hello.42")

    def test_table(self):
        self.assertEqual(120, len(self.morse.table))

    def test_table_reversed(self):
        self.assertEqual(68, len(self.morse.table_reversed))
