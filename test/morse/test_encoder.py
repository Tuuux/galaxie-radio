#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxradio.morse.encoder import MorseEncoder
import unittest
import tempfile
import wave
import os


# Unittest
class TestMorseEncoder(unittest.TestCase):
    def setUp(self):
        self.morse_encoder = MorseEncoder()

    def test_MorseEncoder_get_dit_sign(self):
        self.assertEqual(".", self.morse_encoder.get_dit_sign())

        self.morse_encoder.dit_sign = "_"
        self.assertEqual("_", self.morse_encoder.get_dit_sign())

    def test_MorseEncoder_set_dit_sign(self):
        self.assertEqual(".", self.morse_encoder.dit_sign)

        self.morse_encoder.set_dit_sign("_")
        self.assertEqual("_", self.morse_encoder.dit_sign)

        self.assertRaises(TypeError, self.morse_encoder.set_dit_sign, None)

    def test_MorseEncoder_get_dah_sign(self):
        self.assertEqual("-", self.morse_encoder.get_dah_sign())

        self.morse_encoder.dah_sign = "="
        self.assertEqual("=", self.morse_encoder.get_dah_sign())

    def test_MorseEncoder_set_dah_sign(self):
        self.assertEqual("-", self.morse_encoder.dah_sign)

        self.morse_encoder.set_dah_sign("=")
        self.assertEqual("=", self.morse_encoder.dah_sign)

        self.assertRaises(TypeError, self.morse_encoder.set_dah_sign, None)

    def test_MorseEncoder_encode(self):
        self.assertEqual(".... . .-.. .-.. ---", self.morse_encoder.encode("hello"))

        self.assertEqual("....- ..---", self.morse_encoder.encode("42"))

        self.assertRaises(TypeError, self.morse_encoder.encode, None)
        self.assertRaises(TypeError, self.morse_encoder.encode, "õ")

    def test_MorseEncoder_decode(self):
        self.assertEqual("HELLO", self.morse_encoder.decode(".... . .-.. .-.. ---"))

        self.assertEqual("42", self.morse_encoder.decode("....- ..---"))

        self.assertEqual("HELLO 42", self.morse_encoder.decode(".... . .-.. .-.. ---    ....- ..---"))

        self.assertRaises(TypeError, self.morse_encoder.decode, None)

    def test_MorseEncoder_encode_exact(self):
        self.assertEqual(".", self.morse_encoder.encode_exact("e"))
        self.assertEqual(". . . . -   . . - - -", self.morse_encoder.encode_exact("42"))
        self.assertEqual(". - - . - .", self.morse_encoder.encode_exact("@"))
        self.morse_encoder.use_international_code = True
        self.assertEqual(". . - . .", self.morse_encoder.encode_exact("é"))
        self.assertEqual("- . - . .", self.morse_encoder.encode_exact("ö"))

        self.assertRaises(TypeError, self.morse_encoder.encode_exact, None)
        self.assertRaises(TypeError, self.morse_encoder.encode_exact, "õ")

    def test_MorseEncoder_text_to_morse_for_human(self):
        self.assertEqual(". . . . / . . . .", self.morse_encoder.text_to_morse_for_human("eeee eeee"))
        self.assertEqual("....- ..--- / ....- ..---", self.morse_encoder.text_to_morse_for_human("42 42"))

        self.assertRaises(TypeError, self.morse_encoder.text_to_morse_for_human, None)
        self.assertRaises(TypeError, self.morse_encoder.text_to_morse_for_human, "hello", separator=None)
        self.assertRaises(TypeError, self.morse_encoder.text_to_morse_for_human, "[")

    def test_MorseEncoder_morse_to_text_for_human(self):
        self.assertEqual("EEEE EEEE", self.morse_encoder.morse_to_text_for_human(". . . . / . . . ."))
        self.assertEqual("42 42", self.morse_encoder.morse_to_text_for_human("....- ..--- / ....- ..---"))

        self.assertRaises(TypeError, self.morse_encoder.morse_to_text_for_human, None)
        self.assertRaises(TypeError, self.morse_encoder.morse_to_text_for_human, "hello", separator=None)

    def test_MorseEncoder_text_to_morse_wave(self):
        with MorseEncoder() as morse:
            morse.set_debug(True)
            morse.set_debug_level(2)
            morse.set_verbose(True)
            message = "Hello"
            temporary_file = tempfile.NamedTemporaryFile()
            temporary_filename = temporary_file.name

            morse.text_to_morse_wave(message, temporary_filename)

            try:
                wav = wave.open(temporary_filename, "r")
                check = True
                wav.close()
            except IOError:
                check = False

            self.assertTrue(check)

            # test error
            self.assertRaises(TypeError, morse.text_to_morse_wave, None, temporary_filename)
            self.assertRaises(TypeError, morse.text_to_morse_wave, "õ", temporary_filename)
            self.assertRaises(TypeError, morse.text_to_morse_wave, "Hello", None)
            self.assertRaises(IOError, morse.text_to_morse_wave, "Hello", os.path.realpath("test"))
            self.assertRaises(FileNotFoundError, morse.text_to_morse_wave, "Hello", os.path.realpath("testO1"))
            self.assertRaises(OSError, morse.text_to_morse_wave, "Hello", os.path.realpath("/dev/watchdog"))

            temporary_file.close()

    def test_MorseEncoder_get_message_length(self):
        self.assertEqual(250, self.morse_encoder.get_message_length("PARIS PARIS PARIS PARIS PARIS", word_spaced=True))
        self.assertEqual(243, self.morse_encoder.get_message_length("PARIS PARIS PARIS PARIS PARIS", word_spaced=False))
        self.assertEqual(50, self.morse_encoder.get_message_length("PARIS", word_spaced=True))
        self.assertEqual(43, self.morse_encoder.get_message_length("PARIS", word_spaced=False))

        self.assertRaises(TypeError, self.morse_encoder.get_message_length, None, n=1, word_spaced=True)
        self.assertRaises(TypeError, self.morse_encoder.get_message_length, "PARIS", n=None, word_spaced=True)
        self.assertRaises(TypeError, self.morse_encoder.get_message_length, "PARIS", n=1, word_spaced="42")

    def test_MorseEncoder_text_to_bin(self):
        self.assertEqual("1011101110100010111000101110100010100010101", self.morse_encoder.text_to_bin("PARIS"))
        self.assertEqual("10101010111000101011101110111", self.morse_encoder.text_to_bin("42"))
        self.assertEqual("10000000100000001", self.morse_encoder.text_to_bin("e e e"))

        self.assertRaises(TypeError, self.morse_encoder.text_to_bin, None)

    def test_MorseEncoder_text_normalize_text(self):
        self.assertEqual("e", self.morse_encoder.normalize_text("é"))

        self.assertRaises(TypeError, self.morse_encoder.normalize_text, None)
