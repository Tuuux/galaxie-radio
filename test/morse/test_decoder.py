#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved
import os.path

from glxradio.morse.decoder import MorseDecoder
from collections import deque

import unittest
import wave


class TestMorseDecoder(unittest.TestCase):
    def setUp(self):
        self.morse_decoder = MorseDecoder()

    # Test Lower level thing
    def test_MorseDecoder_get_what_i_display(self):
        self.assertEqual("", self.morse_decoder.get_what_i_display())

        self.morse_decoder.what_i_display = "Hello"
        self.assertEqual("Hello", self.morse_decoder.get_what_i_display())

    def test_MorseDecoder_set_what_i_display(self):
        self.assertEqual("", self.morse_decoder.what_i_display)

        self.morse_decoder.set_what_i_display("Hello")
        self.assertEqual("Hello", self.morse_decoder.what_i_display)

        self.assertRaises(TypeError, self.morse_decoder.set_what_i_display, None)

    def test_MorseDecoder_get_cws(self):
        self.assertEqual("", self.morse_decoder.get_cws())

        self.morse_decoder.cws = "Hello"
        self.assertEqual("Hello", self.morse_decoder.get_cws())

    def test_MorseDecoder_set_cws(self):
        self.assertEqual("", self.morse_decoder.cws)

        self.morse_decoder.set_cws("Hello")
        self.assertEqual("Hello", self.morse_decoder.cws)

        # test error
        self.assertRaises(TypeError, self.morse_decoder.set_cws, None)

    def test_MorseDecoder_get_two_dits_size(self):
        self.assertEqual(960.0, self.morse_decoder.get_two_dits_size())

        self.morse_decoder.two_dits_size = 400.0
        self.assertEqual(400.0, self.morse_decoder.get_two_dits_size())

    def test_MorseDecoder_set_two_dits_size(self):
        self.assertEqual(960, self.morse_decoder.two_dits_size)

        self.morse_decoder.set_two_dits_size(400)
        self.assertEqual(400, self.morse_decoder.two_dits_size)

        # test error
        self.assertRaises(TypeError, self.morse_decoder.set_two_dits_size, None)

    def test_MorseDecoder_get_sigma(self):
        self.assertEqual(0.4, self.morse_decoder.get_sigma())

        self.morse_decoder.sigma = 0.42
        self.assertEqual(0.42, self.morse_decoder.get_sigma())

    def test_MorseDecoder_set_sigma(self):
        self.assertEqual(0.4, self.morse_decoder.sigma)

        self.morse_decoder.set_sigma(0.42)
        self.assertEqual(0.42, self.morse_decoder.sigma)

        self.assertRaises(TypeError, self.morse_decoder.set_sigma, None)

    def test_MorseDecoder_get_dit_size(self):
        self.assertEqual(1200, self.morse_decoder.get_dit_size())

        self.morse_decoder.dit_size = 650
        self.assertEqual(650, self.morse_decoder.get_dit_size())

    def test_MorseDecoder_set_dit_size(self):
        self.assertEqual(1200, self.morse_decoder.dit_size)

        self.morse_decoder.set_dit_size(600)
        self.assertEqual(600, self.morse_decoder.dit_size)

        # test error
        self.assertRaises(TypeError, setattr, self.morse_decoder, "frequency", "Hello.42")

    def test_MorseDecoder_get_wpm_default(self):
        self.assertEqual(20, self.morse_decoder.get_wpm_default())

        self.morse_decoder.wpm_default = 42
        self.assertEqual(42, self.morse_decoder.get_wpm_default())

    def test_MorseDecoder_set_wpm_default(self):
        self.assertEqual(20, self.morse_decoder.wpm_default)

        self.morse_decoder.set_wpm_default(42)
        self.assertEqual(42, self.morse_decoder.wpm_default)

        # test error
        self.assertRaises(TypeError, self.morse_decoder.set_wpm_default, None)

    def test_MorseDecoder_get_wpm_upper(self):
        self.assertEqual(60, self.morse_decoder.get_wpm_upper())

        self.morse_decoder.wpm_upper = 20
        self.assertEqual(20, self.morse_decoder.get_wpm_upper())

    def test_MorseDecoder_set_wpm_upper(self):
        self.assertEqual(60, self.morse_decoder.wpm_upper)

        self.morse_decoder.set_wpm_upper(42)
        self.assertEqual(42, self.morse_decoder.wpm_upper)

        self.assertRaises(TypeError, self.morse_decoder.set_wpm_upper, None)

    def test_MorseDecoder_get_threshold_upper(self):
        self.assertEqual(0.5, self.morse_decoder.get_threshold_upper())

        self.morse_decoder.threshold_upper = 1.0
        self.assertEqual(1.0, self.morse_decoder.get_threshold_upper())

    def test_MorseDecoder_set_threshold_upper(self):
        self.assertEqual(0.5, self.morse_decoder.threshold_upper)

        self.morse_decoder.set_threshold_upper(42)
        self.assertEqual(0.42, self.morse_decoder.threshold_upper)

        self.assertRaises(TypeError, self.morse_decoder.set_threshold_upper, None)

    def test_MorseDecoder_get_threshold_lower(self):
        self.assertEqual(0.5, self.morse_decoder.get_threshold_lower())

        self.morse_decoder.threshold_lower = 1.0
        self.assertEqual(1.0, self.morse_decoder.get_threshold_lower())

    def test_MorseDecoder_set_threshold_lower(self):
        self.assertEqual(0.5, self.morse_decoder.threshold_lower)

        self.morse_decoder.set_threshold_lower(42)
        self.assertEqual(0.42, self.morse_decoder.threshold_lower)

        self.assertRaises(TypeError, self.morse_decoder.set_threshold_lower, None)

    # Auto Gain Control test
    def test_MorseDecoder_get_auto_gain_control(self):
        self.assertEqual(False, self.morse_decoder.get_auto_gain_control())

        self.morse_decoder.auto_gain_control = True
        self.assertEqual(True, self.morse_decoder.get_auto_gain_control())

    def test_MorseDecoder_set_auto_gain_control(self):
        self.assertEqual(False, self.morse_decoder.auto_gain_control)

        self.morse_decoder.set_auto_gain_control(True)
        self.assertEqual(True, self.morse_decoder.auto_gain_control)

        self.assertRaises(TypeError, self.morse_decoder.set_auto_gain_control, None)

    def test_MorseDecoder_get_auto_gain_control_queue_size(self):
        self.assertEqual(10, self.morse_decoder.get_auto_gain_control_queue_size())

        self.morse_decoder.auto_gain_control_queue_size = 20
        self.assertEqual(20, self.morse_decoder.get_auto_gain_control_queue_size())

    def test_MorseDecoder_set_auto_gain_control_queue_size(self):
        self.assertEqual(10, self.morse_decoder.auto_gain_control_queue_size)

        self.morse_decoder.set_auto_gain_control_queue_size(42)
        self.assertEqual(42, self.morse_decoder.auto_gain_control_queue_size)

        self.assertRaises(TypeError, self.morse_decoder.set_auto_gain_control_queue_size, None)

    def test_MorseDecoder_get_auto_gain_control_queue(self):
        self.assertEqual(deque, type(self.morse_decoder.get_auto_gain_control_queue()))

        self.morse_decoder.auto_gain_control_queue = None
        self.assertEqual(None, self.morse_decoder.get_auto_gain_control_queue())

    def test_MorseDecoder_set_auto_gain_control_queue(self):
        self.assertEqual(deque, type(self.morse_decoder.auto_gain_control_queue))

        self.morse_decoder.auto_gain_control_queue = None
        self.assertEqual(None, self.morse_decoder.auto_gain_control_queue)
        self.morse_decoder.set_auto_gain_control_queue()
        self.assertEqual(deque, type(self.morse_decoder.auto_gain_control_queue))

        self.morse_decoder.set_auto_gain_control_queue(deque())
        self.assertEqual(deque, type(self.morse_decoder.auto_gain_control_queue))

        self.assertRaises(TypeError, self.morse_decoder.set_auto_gain_control_queue, float(42))

    def test_MorseDecoder_get_auto_gain_control_rolling(self):
        self.morse_decoder.set_auto_gain_control_queue(deque())
        self.morse_decoder.set_auto_gain_control_queue_size(10)
        # 10 time
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        self.morse_decoder.get_auto_gain_control_rolling(10)
        value = self.morse_decoder.get_auto_gain_control_rolling(10)
        self.assertEqual(10, value)

    def test_MorseDecoder_get_auto_gain_control_decay(self):
        weight = 1.0
        average = 4200
        value = 42

        self.assertEqual(42, self.morse_decoder.get_auto_gain_control_decay(average, value, weight))

        weight = 4200
        average = 420
        value = 42
        need_value_1 = float(value * (1.0 / weight) + average * (1.0 - (1.0 / weight)))

        self.assertEqual(
            need_value_1,
            self.morse_decoder.get_auto_gain_control_decay(average, value, weight),
        )

    # Big
    def test_MorseDecoder_process(self):

        # wave.open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'galaxie-radio.wav'))
        # wav = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'galaxie-radio.wav')

        with MorseDecoder() as morse_decoder:
            morse_decoder.debug = True
            morse_decoder.debug_level = 2
            morse_decoder.verbose = True
            print(os.path.join(os.path.dirname(os.path.abspath(__file__)), "galaxie-radio.wav"))
            # Analyse the wav file
            decoded_message = morse_decoder.process(
                filename=os.path.join(os.path.dirname(os.path.abspath(__file__)), "galaxie-radio.wav")
            )

            self.assertEqual("GALAXIE-RADI", decoded_message)
