.. title:: Galaxie Radio, Amateur Radio ToolKit

============================
Galaxie Radio Documentation
============================
.. figure::  images/logo_galaxie.png
   :align:   center

Actual Homepage : https://gitlab.com/Tuuux/galaxie-radio

Documentation : https://galaxie-radio.readthedocs.io

The Project
-----------
**Galaxie Radio** is a free software ToolKit for Amateur Radio.

Originally the project have start in 2018 when the author Jérôme.O Alias Tuuux have start to learn Radio Communication.

The Mission
-----------
Provide a Text Based ToolKit for assist Amateur Radio.

Example
-------

.. code-block:: python

   #!/usr/bin/env python
   # -*- coding: utf-8 -*-

   # It script it publish under GNU GENERAL PUBLIC LICENSE
   # http://www.gnu.org/licenses/gpl-3.0.en.html
   # Author: the Galaxie Radio Team, all rights reserved
   import os
   import sys

   # Require when you haven't GLXRadio as default Package
   current_dir = os.path.dirname(os.path.abspath(__file__))
   sys.path.append(os.path.dirname(current_dir))

   import GLXRadio

   my_csv_info = list()
   #########################################
   # PMR446                                #
   #########################################

   generator_pmr446 = GLXRadio.PMR446()
   generator_pmr446.suffix_name = 'PMR'

   # Phonie / Survivaliste / Preppers
   for info_channel in generator_pmr446.get_info_channels(
           digital=False,
           channels=[1, 2, 3, 4],
           tones=None
   ):
       my_csv_info.append(info_channel)

   # Phonie - Canal Scout - Randonnée / Radioscoutisme
   for info_channel in generator_pmr446.get_info_channels(
           digital=False,
           channels=[5],
           tones=[1, 2, 3, 4, 5, 6, 7, 8, 42]
   ):
       my_csv_info.append(info_channel)

   # Répéteurs / CTCSS Recommandé
   for info_channel in generator_pmr446.get_info_channels(
           digital=False,
           tone='CTCSS',
           channels=[6],
           tones=None
   ):
       my_csv_info.append(info_channel)

   # Balises/Modes Digitaux/URGENCE MONTAGNE / URG. MONTAGNE = Ch 7 tone 7
   for info_channel in generator_pmr446.get_info_channels(
           digital=False,
           channels=[7],
           tones=[7]
   ):
       my_csv_info.append(info_channel)

   # CANAL D'APPEL (Local et DX) / Aucun CTCSS ou 8
   for info_channel in generator_pmr446.get_info_channels(
           digital=False,
           channels=[8],
           tones=[8]
   ):
       my_csv_info.append(info_channel)

   csv_generator = GLXRadio.CSV()

   # With location
   info_with_location = csv_generator.add_location(my_csv_info)

   # With Frist line
   info_with_first_line = csv_generator.add_first_line(info_with_location)

   # Print
   csv_generator.flush(info_with_first_line)

   sys.exit(0)

Output
------
 Location,Name,Frequency,Duplex,Offset,Tone,rToneFreq,cToneFreq,DtcsCode,DtcsPolarity,Mode,TStep,Skip,Comment,URCALL,RPT1CALL,RPT2CALL
 0,PMR 1,446.006250,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 1,PMR 2,446.018750,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 2,PMR 3,446.031250,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 3,PMR 4,446.043750,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 4,PMR 5,446.056250,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 5,PMR 5.1,446.056250,,0.000000,Tone,67.0,67.0,023,NN,NFM,6.25,S,,,,
 6,PMR 5.2,446.056250,,0.000000,Tone,71.9,71.9,023,NN,NFM,6.25,S,,,,
 7,PMR 5.3,446.056250,,0.000000,Tone,74.4,74.4,023,NN,NFM,6.25,S,,,,
 8,PMR 5.4,446.056250,,0.000000,Tone,77.0,77.0,023,NN,NFM,6.25,S,,,,
 9,PMR 5.5,446.056250,,0.000000,Tone,79.7,79.7,023,NN,NFM,6.25,S,,,,
 10,PMR 5.6,446.056250,,0.000000,Tone,82.5,82.5,023,NN,NFM,6.25,S,,,,
 11,PMR 5.7,446.056250,,0.000000,Tone,85.4,85.4,023,NN,NFM,6.25,S,,,,
 12,PMR 5.8,446.056250,,0.000000,Tone,88.5,88.5,023,NN,NFM,6.25,S,,,,
 13,PMR 5.42,446.056250,,0.000000,DTCS,67.0,67.0,311,NN,NFM,6.25,S,,,,
 14,PMR 6,446.068750,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 15,PMR 7,446.081250,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 16,PMR 7.7,446.081250,,0.000000,Tone,85.4,85.4,023,NN,NFM,6.25,S,,,,
 17,PMR 8,446.093750,,0.000000,,67.0,67.0,023,NN,NFM,6.25,,,,,
 18,PMR 8.8,446.093750,,0.000000,Tone,88.5,88.5,023,NN,NFM,6.25,S,,,,


More examples can be found here: https://gitlab.com/Tuuux/galaxie-radio/tree/master/examples

Features
--------
* Generate CSV file for GRIP

Contribute
----------

- Issue Tracker: https://gitlab.com/Tuuux/galaxie-radio/issues
- Source Code: https://gitlab.com/Tuuux/galaxie-radio

Our collaboration model is the Collective Code Construction Contract (C4): https://rfc.zeromq.org/spec:22/C4/

Documentations
--------------
.. toctree::
   :maxdepth: 2

   install
   modules
   Basic-Types

License
-------
GNU GENERAL PUBLIC LICENSE Version 3


See the LICENCE_

.. _LICENCE: https://gitlab.com/Tuuux/galaxie-radio/blob/master/LICENSE

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------
* :ref:`genindex`
* :ref:`search`